# zup-dribbble.ts

Teste para Front-End Developer recriando funcionalidades do site dribbble.com utilizando sua API Pública.

## Instalação

Faça o clone deste repositório e execute na pasta raiz do projeto utilizando terminal ou prompt:

* `npm i -g gulp` para instalar o gulp globalmente (se você já não o tiver instalado)
* `npm install` para instalar dependências npm locais

## Rodar a Aplicação

Após completar a instalação execute na pasta raiz o seguinte comando no terminal ou prompt:

* `gulp start` para iniciar a aplicação no browser. Os arquivos das aplicações são observados e serão re-compilados em cada alteração.
